//
//  NimbleAssignmentUITests.swift
//  NimbleAssignmentUITests
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import XCTest

class NimbleAssignmentUITests: XCTestCase {
    
    var app : XCUIApplication!

    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        app = XCUIApplication()
    }
    
    override func tearDown() {
        app = nil
        super.tearDown()
    }
    
    func testCollectionViewElements() {
        app.launch()
        
        sleep(1)
        XCTAssertTrue(app.navigationBars.firstMatch.buttons["Refresh"].exists)
        XCTAssertTrue(app.navigationBars.firstMatch.buttons["Bookmarks"].exists)
        XCTAssertTrue(app.navigationBars["SURVEYS"].exists)
        XCTAssertTrue(app.collectionViews.firstMatch.exists)
        
        let cell = app.collectionViews.element(boundBy: 0).cells.firstMatch
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: cell, handler: nil)
        waitForExpectations(timeout: 3, handler: nil)
        
        XCTAssertTrue(app.collectionViews.firstMatch.cells.count > 0)
        XCTAssertTrue(cell.images.firstMatch.exists)
    }
    
    func testSurveyDetailsNavigation() {
        app.launch()
        
        let cell = app.collectionViews.element(boundBy: 0).cells.firstMatch
        let exists = NSPredicate(format: "exists == true")
        expectation(for: exists, evaluatedWith: cell, handler: nil)
        waitForExpectations(timeout: 3, handler: nil)
        
        cell.buttons.firstMatch.tap()
        XCTAssertFalse(app.navigationBars["SURVEYS"].exists)
        XCTAssertFalse(app.collectionViews.firstMatch.exists)
        
        app.navigationBars.firstMatch.buttons["SURVEYS"].tap()
        XCTAssertTrue(app.navigationBars["SURVEYS"].exists)
        XCTAssertTrue(app.collectionViews.firstMatch.exists)
    }

}
