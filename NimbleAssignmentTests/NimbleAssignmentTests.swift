//
//  NimbleAssignmentTests.swift
//  NimbleAssignmentTests
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import XCTest
import Alamofire
@testable import NimbleAssignment

class NimbleAssignmentTests: XCTestCase {

    func testGetSurveys() {
        let mockService = MockDataService()
        mockService.completeSurveys = .success([Survey.with()])
        
        let viewModel = SurveysViewModel(dataService: mockService)
        viewModel.getSurveys()
        
        XCTAssertNotNil(viewModel.surveys.first)
    }
    
    func testCreateCellModels() {
        let mockService = MockDataService()
        let survey = Survey.with()
        
        let viewModel = SurveysViewModel(dataService: mockService)
        
        let cellModel = viewModel.createCellViewModel(survey: survey)
        
        XCTAssertTrue(cellModel.title == survey.title)
        XCTAssertTrue(cellModel.description == survey.description)
        XCTAssertTrue(cellModel.imageURL == URL(string: survey.coverURL))
    }
    
}

private class MockDataService: DataServiceProtocol {
    var completeSurveys: Result<[Survey]>?
    
    func getToken(completion: @escaping (String?) -> ()) {
        completion("")
    }
    func getSurveys(completion: @escaping (Result<[Survey]>) -> ()) {
        completion(completeSurveys!)
    }
}

extension Survey {
    static func with(id: String = "1",
                     title: String = "Test survey",
                     description: String = "description of survey",
                     coverURL: String = "httpt://image.com") -> Survey {
        return Survey(id: id, title: title, description: description, coverURL: coverURL)
    }
}
