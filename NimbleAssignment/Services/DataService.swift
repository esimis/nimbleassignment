//
//  DataService.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import Foundation
import Alamofire
import Locksmith

protocol DataServiceProtocol {
    func getToken(completion: @escaping (String?) -> ())
    func getSurveys(completion: @escaping (Result<[Survey]>) -> ())
}

class DataService: DataServiceProtocol {
    
    // MARK: - Singleton
    static let shared = DataService()
    
    private var oAuthToken: String? {
        set {
            if let valueToSave = newValue {
                do {
                    try Locksmith.saveData(data: ["token": valueToSave], forUserAccount: "nimble")
                } catch {
                    try? Locksmith.deleteDataForUserAccount(userAccount: "nimble")
                }
            }
            else {
                try? Locksmith.deleteDataForUserAccount(userAccount: "nimble")
            }
        }
        get {
            let dictionary = Locksmith.loadDataForUserAccount(userAccount: "nimble")
            if let token =  dictionary?["token"] as? String {
                return token
            }
            return nil
        }
    }
    
    // MARK: - Services
    func getToken(completion: @escaping (String?) -> ()) {
        let url = "\(Service.baseURL)\(Service.tokenPath)"
        let parameters = ["grant_type" : "password",
                          "username" : Service.username,
                          "password" : Service.password]
        AF.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default).validate().responseDecodable { (response: DataResponse<Token>) in
            //TODO: error handling for token call
            self.oAuthToken = response.value?.accessToken
            completion(response.value?.accessToken)
            
        }
    }
    
    func getSurveys(completion: @escaping (Result<[Survey]>) -> ()) {
        self.getToken { (token) in
            if let token = token {
                let url = "\(Service.baseURL)/surveys.json"
                let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
                AF.request(url, headers: headers).validate().responseDecodable { (response: DataResponse<[Survey]>) in
                    switch response.result {
                    case .success:
                        completion(response.result)
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
            } else {
                completion(.failure(ServiceError.missingToken))
            }
        }
    }
}
