//
//  ViewController.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import UIKit
import SnapKit

class SurveyListViewController: UIViewController {

    weak var collectionView: UICollectionView!
    weak var pageControl: UIPageControl!
    weak var activityIndicator: UIActivityIndicatorView!
    
    let viewModel = SurveysViewModel(dataService: DataService())
    
    override func viewDidAppear(_ animated: Bool) {
        self.pageControl.customPageControl(dotFillColor: .white, dotBorderColor: .white, dotBorderWidth: 1)
    }
    
    override func loadView() {
        super.loadView()
        
        self.layoutViews()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.viewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.viewModel.isLoading ?? false
                if isLoading {
                    self?.activityIndicator.startAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.collectionView.alpha = 0.0
                    })
                }else {
                    self?.activityIndicator.stopAnimating()
                    UIView.animate(withDuration: 0.2, animations: {
                        self?.collectionView.alpha = 1.0
                    })
                }
            }
        }
        self.viewModel.reloadCollectionViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.pageControl.numberOfPages = self?.viewModel.surveys.count ?? 0
                self?.collectionView.reloadData()
                self?.collectionView.setContentOffset(CGPoint(x:0,y:0), animated: true)
            }
        }
        self.viewModel.getSurveys()
    }
    
    func layoutViews() {
        // Collection view
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        self.collectionView = collectionView
        
        // Page control
        let pageControl = UIPageControl(frame: .zero)
        pageControl.numberOfPages = self.viewModel.surveys.count
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.numberOfPages = 10
        self.view.addSubview(pageControl)
        pageControl.transform = CGAffineTransform(rotationAngle: .pi/2).scaledBy(x: 2, y: 2)
        pageControl.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        self.pageControl = pageControl
        
        //Activity indicator
        let activity = UIActivityIndicatorView(style: .whiteLarge)
        activity.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(activity)
        activity.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
        self.activityIndicator = activity
    }
    
    func setupViews() {
        self.collectionView.backgroundColor = .white
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.isPagingEnabled = true
        self.collectionView.showsHorizontalScrollIndicator = false
        self.collectionView.showsVerticalScrollIndicator = false
        self.collectionView.contentInsetAdjustmentBehavior = .never
        self.collectionView.register(SurveyCollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
        
        self.title = "SURVEYS"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshPressed))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.bookmarks, target: self, action: #selector(menuPressed))
    }
    
    @objc func refreshPressed() {
        self.viewModel.getSurveys()
    }
    
    @objc func menuPressed() { }
    
}

extension SurveyListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.surveys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath) as! SurveyCollectionViewCell
        cell.surveyCellViewModel = self.viewModel.surveys[indexPath.item]
        cell.delegate = self
        return cell
    }
}

extension SurveyListViewController: SurveyCollectionViewCellDelegate {
    func didPressButton(title: String?) {
        let vc = SurveyDetailsViewController()
        vc.titleString = title
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension SurveyListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.item
    }
}

extension SurveyListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.bounds.size.width, height: collectionView.bounds.size.height)
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
    }
}

