//
//  SurveyDetailsViewController.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import UIKit

class SurveyDetailsViewController: UIViewController {
    
    var titleString: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = titleString
        self.view.backgroundColor = .white
    }
}
