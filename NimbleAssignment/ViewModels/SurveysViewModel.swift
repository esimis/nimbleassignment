//
//  SurveysViewModel.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import Foundation

class SurveysViewModel {
    
    var surveys = [SurveyCollectionCellViewModel]() {
        didSet {
            self.reloadCollectionViewClosure?()
        }
    }
    
    var error: Error? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    private var dataService: DataServiceProtocol
    
    // MARK: - Closures for callback
    var showAlertClosure: (() -> ())?
    var reloadCollectionViewClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    
    // MARK: - Constructor
    init(dataService: DataServiceProtocol) {
        self.dataService = dataService
    }
    
    // MARK: getSurveys
    func getSurveys() {
        self.isLoading = true
        self.dataService.getSurveys { (result) in
            self.isLoading = false
            if let error = result.error {
                self.error = error
                return
            }
            self.error = nil
            if let surveys = result.value {
                var surveyViewModels = [SurveyCollectionCellViewModel]()
                for survey in surveys {
                    surveyViewModels.append(self.createCellViewModel(survey: survey))
                }
                self.surveys = surveyViewModels
            }
        }
    }
    
    func createCellViewModel(survey: Survey) -> SurveyCollectionCellViewModel {
        let url = URL(string: survey.coverURL)
        return SurveyCollectionCellViewModel(imageURL: url, title: survey.title, description: survey.description)
    }
}

//MARK: ViewModel for collection view cell
struct SurveyCollectionCellViewModel {
    let imageURL: URL?
    let title: String
    let description: String
}
