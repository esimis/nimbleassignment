//
//  Survey.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import Foundation

struct Survey: Codable {
    let id: String
    let title: String
    let description: String
    let coverURL: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case description
        case coverURL = "cover_image_url"
    }
}
