//
//  UIPageControlExtension.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import UIKit

extension UIPageControl {
    
    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
        for (pageIndex, dotView) in self.subviews.enumerated() {
            dotView.layer.cornerRadius = dotView.frame.size.height / 2
            dotView.layer.borderColor = dotBorderColor.cgColor
            dotView.layer.borderWidth = dotBorderWidth
            if self.currentPage == pageIndex {
                dotView.backgroundColor = dotFillColor
            }else{
                dotView.backgroundColor = .clear
            }
        }
    }
}
