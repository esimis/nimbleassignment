//
//  SurveyCollectionViewCell.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import UIKit
import SDWebImage

protocol SurveyCollectionViewCellDelegate {
    func didPressButton(title: String?)
}

class SurveyCollectionViewCell: UICollectionViewCell {
    weak var titleLabel: UILabel!
    weak var descriptionLabel: UILabel!
    weak var bgImage: UIImageView!
    weak var takeSurveyButton: UIButton!
    
    var delegate:  SurveyCollectionViewCellDelegate?
    
    var surveyCellViewModel: SurveyCollectionCellViewModel? {
        didSet {
            self.bgImage.sd_setImage(with: surveyCellViewModel?.imageURL, completed: nil)
            self.titleLabel.text = surveyCellViewModel?.title
            self.descriptionLabel.text = surveyCellViewModel?.description
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Background image
        let bgImage = UIImageView(frame: .zero)
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(bgImage)
        bgImage.snp.makeConstraints { (make) in
            make.top.bottom.left.right.equalToSuperview()
        }
        bgImage.contentMode = .scaleAspectFit
        self.bgImage = bgImage
        
        // Title label
        let titleLabel = UILabel(frame: .zero)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().inset(200)
            make.centerX.equalToSuperview()
        }
        titleLabel.textColor = .white
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: 25)
        self.titleLabel = titleLabel
        
        // Description label
        let descriptionLabel = UILabel(frame: .zero)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(8)
        }
        descriptionLabel.textColor = .white
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = UIFont.systemFont(ofSize: 18)
        self.descriptionLabel = descriptionLabel
        
        // Survey button
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().inset(100)
            make.centerX.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(200)
        }
        button.setTitle("Take the survey", for: .normal)
        button.backgroundColor = .red
        button.titleLabel?.textColor = .white
        button.layer.cornerRadius = 20
        button.clipsToBounds = true
        button.addTarget(self, action: #selector(surveyButtonPressed), for: .touchUpInside)
        self.takeSurveyButton = button
        
        self.contentView.backgroundColor = .lightGray
    }
    
    @objc func surveyButtonPressed() {
        self.delegate?.didPressButton(title: self.surveyCellViewModel?.title)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = nil
        self.descriptionLabel.text = nil
        self.bgImage.image = nil
    }
}
