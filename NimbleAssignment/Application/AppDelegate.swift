//
//  AppDelegate.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if window == nil {
            window = UIWindow(frame: UIScreen.main.bounds)
        }
        
        let viewController = SurveyListViewController()
        viewController.view.backgroundColor = .gray
        let navigationController = UINavigationController(rootViewController: viewController)
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().barTintColor = UIColor(red: 15/255, green: 2/255, blue: 50/255, alpha: 1)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
    }

}

