//
//  Constants.swift
//  NimbleAssignment
//
//  Created by Edgars Simanovskis on 29/03/19.
//  Copyright © 2019 Edgars Simanovskis. All rights reserved.
//

import Foundation

struct Service {
    static let baseURL = "https://nimble-survey-api.herokuapp.com"
    static let tokenPath = "/oauth/token"
    static let username = "carlos@nimbl3.com"
    static let password = "antikera"
}

enum ServiceError: Error {
    case missingToken
}
